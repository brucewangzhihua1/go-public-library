###

测试用例：
```
import (
	"fmt"
	"testing"
	
	"gitee.com/brucewangzhihua1/go-public-library/config"
	"gitee.com/brucewangzhihua1/go-public-library/config/source/file"
)

func TestApp(t *testing.T)  {
	c, err := config.NewConfig()
	if err != nil {
		t.Error(err)
	}
	err = c.Load(file.NewSource(file.WithPath("config/settings.yml")))
	if err != nil {
		t.Error(err)
	}
	fmt.Println(c.Map())
}
```
