module gitee.com/brucewangzhihua1/go-public-library/plugins/logger/logrus

go 1.14

require (
	gitee.com/brucewangzhihua1/go-public-library
	github.com/sirupsen/logrus v1.8.0
)

replace gitee.com/brucewangzhihua1/go-public-library => ../../../
