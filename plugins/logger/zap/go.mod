module gitee.com/brucewangzhihua1/go-public-library/plugins/logger/zap

go 1.14

require (
	gitee.com/brucewangzhihua1/go-public-library v1.3.5-rc.5
	go.uber.org/zap v1.10.0
)

replace gitee.com/brucewangzhihua1/go-public-library => ../../../
